<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('/master', function(){
//     return view('layout.master');
// });

Route::group(['middleware' =>['auth']], function () {
    /*
    Route::get('/kategori', 'KategoriController@index');
    Route::get('/kategori/create', 'KategoriController@create');
    Route::post('/kategori', 'KategoriController@store');
    Route::get('/kategori/{kategori_id}', 'KategoriController@show');
    Route::get('/kategori/{kategori_id}/edit', 'KategoriController@edit');
    Route::put('/kategori/{kategori_id}', 'KategoriController@update');
    Route::delete('/kategori/{kategori_id}', 'KategoriController@destroy');    
    */

    //Route Kategori
    Route::resource('kategori', 'KategoriController');
    Route::resource('profile', 'ProfileController')->only([
        'index', 'update'
    ]);

    Route::resource('komentar', 'KomentarController')->only([
        'store'
    ]);
});

//CRUD Berita
/*
Route::get('/berita', 'BeritaController@index');
Route::get('/berita/create', 'BeritaController@create');
Route::post('/berita', 'BeritaController@store');
Route::get('/berita/{berita_id}', 'BeritaController@show');
Route::get('/berita/{berita_id}/edit', 'BeritaController@edit');
Route::put('/berita/{berita_id}', 'BeritaController@update');
Route::delete('/berita/{berita_id}', 'BeritaController@destroy');
*/
 
//Route Berita
Route::resource('berita', 'BeritaController');

Auth::routes();

// Route::get('/', 'HomeController@index')->name('home');
Route::get('/', 'BeritaController@index');