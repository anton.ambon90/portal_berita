<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Komentar;
use Illuminate\Support\Facades\Auth;

class KomentarController extends Controller
{
    public function store(Request $request){
        $request->validate([
            'isi' => 'required',
        ]);

        $komentar = new Komentar;

        $komentar->isi = $request->isi;
        $komentar->users_id = Auth::id();
        $komentar->berita_id = $request->berita_id;

        $komentar->save();

        return redirect()->back();
    }
}
