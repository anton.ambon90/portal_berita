<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Profile;

class ProfileController extends Controller
{
    public function index(){
        $profile = Profile::where('users_id', Auth::id())->first();

        return view('profile.index', compact('profile'));
    }

    public function update($id, Request $request){
        $request->validate([
            'umur' => 'required',
            'alamat' => 'required',
            'bio' => 'required',

        ]);

        $profile = Profile::find($id);

        $profile->umur = $request['umur'];
        $profile->alamat = $request['alamat'];
        $profile->bio = $request['bio'];

        $profile->save();

        return redirect('/profile');

    }
}