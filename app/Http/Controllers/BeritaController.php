<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Berita;
use App\Kategori;
use File;

class BeritaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);  
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $berita = Berita::all();
        
        return view('berita.index', compact('berita'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $kategori = DB::table('kategori')->get();

        $kategori = Kategori::all();
        return view('berita.create',compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'judul' => 'required',
            'content' => 'required',
            'thumbnail' => 'required|image|mimes:jpeg,png,jpg,|max:2048',
            'kategori_id' => 'required',
        ]);

        $thumbnailName = time().'.'.$request->thumbnail->extension();

        $request->thumbnail->move(public_path('thumbnail'), $thumbnailName);
        
        $berita = new Berita;

        $berita->judul = $request->judul;
        $berita->content = $request->content;
        $berita->thumbnail = $thumbnailName;
        $berita->kategori_id = $request->kategori_id;

        $berita->save();

        return redirect('/berita');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $berita = Berita::findOrFail($id);

        return view('berita.show', compact('berita'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $kategori = DB::table('kategori')->get();
        $kategori = Kategori::all();
        $berita = Berita::findOrFail($id);

        return view('berita.edit', compact('berita','kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'content' => 'required',
            'kategori_id' => 'required',
            'thumbnail' => 'required|image|mimes:jpeg,png,jpg,|max:2048',
            
        ]);
        
        

        if($request->has('thumbnail')) {
            $berita = Berita::find($id);
            
            $path = "thumbnail/";
            File::delete($path . $berita->thumbnail);


            $thumbnailName = time().'.'.$request->thumbnail->extension();

            $request->thumbnail->move(public_path('thumbnail'), $thumbnailName);
            
    
            $berita->judul = $request->judul;
            $berita->content = $request->content;
            $berita->kategori_id = $request->kategori_id;
            $berita->thumbnail = $thumbnailName;

            $berita->save();
            return redirect('/berita');

        }else{
            $berita = Berita::find($id);
            $berita->judul = $request->judul;
            $berita->content = $request->content;
            $berita->kategori_id = $request->kategori_id;

            $berita->save();
            return redirect('/berita');


        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $berita = Berita::find($id);

        $path = "thumbnail/";
        File::delete($path . $berita->thumbnail);

        $berita->delete();

        return redirect('/berita');
    }
}