<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    protected $table = 'komentar';
    protected $fillable = ['isi', 'users_id', 'berita_id'];

    public function users()
    {
        return $this->belongsTo('App\User');
    }

    public function berita()
    {
        return $this->belongsTo('App\Berita');
    }
}
