@extends('layout.master')

@section('judul')
    Edit Kategori {{$kategori->name}}
@endsection

@section('content')
<form action="/kategori/{{$kategori->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>Nama Kategori</label>
      <input type="text" name="name" value="{{$kategori->name}}" class="form-control">
    </div>
    @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Deskripsi</label>
      <textarea name="deskripsi" class="form-control" cols="30" rows="10">{{$kategori->deskripsi}}</textarea>
    </div>
    @error('deskripsi')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection