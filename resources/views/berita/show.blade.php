@extends('layout.master')

@section('judul')
    Detail Berita {{ $berita->judul }}
@endsection

@section('content')

    <img src="{{ asset('thumbnail/' . $berita->thumbnail) }}" alt="gambar berita">
    <h1>{{ $berita->judul }}</h1>
    <p>{{ $berita->content }}</p>

    <p>tanggal dibuat : {{ $berita->created_at }}</p>

    <h2><b>Komentar</b></h2>

    @forelse ($berita->komentar as $item)
        <div class="card" style="width: 18rem;">
            <div class="card-body">
                <small><b>{{$item->users->name}}</b></small>
                <p class="card-text">{{$item->isi}}</p>
            </div>
        </div>

    @empty
        <h4>Tidak Ada Komentar</h4>
    @endforelse

    <form action="/komentar" class="my-4" method="POST">
        @csrf
        <div class="form-group">
            <label>Komentar</label>
            <input type="hidden" value="{{ $berita->id }}" name="berita_id">
            <textarea name="isi" class="form-control" cols="30" rows="10"></textarea>
            @error('isi')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

    <a href="/berita" class="btn btn-secondary">Back</a>

@endsection
