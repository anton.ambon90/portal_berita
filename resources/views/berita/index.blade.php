@extends('layout.master')

@section('judul')
    List Berita
@endsection

@section('content')

@auth
<a href="/berita/create" class="btn btn-primary my-2">Tambah Berita</a>
   
@endauth

    <div class="row">
        @forelse ($berita as $item)
            <div class="col-4">
                <div class="card" style="width: 18rem;">
                    <img src="{{asset('thumbnail/'. $item->thumbnail)}}" class="card-img-top" alt="...">
                    
                    <div class="card-body">
                        <span class="badge badge-info">{{$item->kategori->name }}</span> <br>
                        <h3 class="card-title">{{$item->judul}}</h3>
                        <p class="card-text">{{Str::limit($item->content, 30)}}</p>
                        @auth
                        <form action="berita/{{$item->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <a href="/berita/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <a href="/berita/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                            <input type="submit" value="Delete" class="btn btn-danger btn-sm">                                
                        </form> 
                        @endauth

                        @guest
                        <a href="/berita/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>

                        @endguest
                    </div>
                </div>
            </div>
        @empty
            <h4>Berita Belum Ada</h4>
        @endforelse

    </div>

@endsection
