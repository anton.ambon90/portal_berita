@extends('layout.master')


@section('judul')
    Update Profile
@endsection

@push('script')
<script src="https://cdn.tiny.cloud/1/f33tbfy6v0vfcytbw8hmj0dgomay94duusqjjy1k0hk445mf/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
  tinymce.init({
    selector: 'textarea',
    plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
    toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
    toolbar_mode: 'floating',
    tinycomments_mode: 'embedded',
    tinycomments_author: 'Author name',
  });
</script>
@endpush



@section('content')
<form action="/profile/{{ $profile->id }}" method="POST">
    @csrf
    @method('PUT')

    <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control" value='{{ $profile->user->name }}' disabled>
      </div>

      <div class="form-group">
        <label>Email</label>
        <input type="text" class="form-control" value='{{ $profile->user->email }}'disabled>
      </div>


    <div class="form-group">
      <label>Umur</label>
      <input type="text" name="umur" class="form-control" value='{{ $profile->umur }}'>
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Alamat</label>
      <textarea name="alamat" class="form-control" >{{ $profile->alamat }}</textarea>
    </div>
    @error('alamat')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Biodata</label>
        <textarea name="bio" class="form-control">{{ $profile->bio }}</textarea>
      </div>
      @error('bio')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror

    <button type="submit" class="btn btn-primary">Update</button>
  </form>
@endsection